"""
Custom Exceptions for the App
"""


class UserDatabaseException(Exception):
    pass


class PoolsDatabaseException(Exception):
    pass


class AmenitiesDatabaseException(Exception):
    pass


class PoolAmenityDatabaseException(Exception):
    pass


class ReservationDatabaseException(Exception):
    pass
