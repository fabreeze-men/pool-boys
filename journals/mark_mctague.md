
### 5/28/24

Mob coded to add some attributes to User Models and Queries. Also made sure to get the docker yaml file working by creating the .env file.


### 5/29/24

Had to start project over due to issues with user models, sign in, out, authenticate.

### 5/30/24

Worked on finishing user and authentication. Got them working properly.  Also worked on db models and SQL code for db tables. Started working on creating a pool also. ended the day without getting it working but I worked on it after hours and figured out the problem. Kept getting a database error. was checking the code we did and made some corrections but main problem was models needed to be fixed, id for pools was not serialized and poolowner_id was not correctly matching up i believe. Will have to avoid doing this in the future though. Need to work with my team on these problems.

### 6/7/24

Worked on getting reservations completed and did some error catching. will be working on testing next class and looking to get redux setup over the weekend with the team.

### 6/17/24

Was able to get more done on functionality on the website. CSS still needs to be done but everything seems to be coming together. I do need to keep up with journals for this last week. Must get into the habit of documenting everything I do

### 6/18/24

Got started on CSS today. Got two pages finished which should be enough to copy and paste a lot of the Tailwind code that we created. Didn't feel like I did mush today though. Need to learn more CSS though and practice with it. I hope to get more time practice but i'm feeling slow to learn and understand things working in a group.

### 6/21/24

worked on fixing more issues and testing the website. fixed things like making certain form inputs required and fixing errors. Did work on getting stuff together for te readme file also and hoping to get that done this weekend so come Monday is just about submitting the project and maybe planning additional features we want to implement or build out.

### 6/22/24

Worked on getting the readme done. Need to work on the instructions to initialize the project but it should be good. We are also going to look at Cosimos reservation calender that he created on the 21st to see if we want to merge it with main when we submit the project. Other then that updated documents we needed, such as wire frame and API end points. Hoping Monday we will just need to settle on a few things.
