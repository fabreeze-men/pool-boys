May 28, 2024
•	Today, we reviewed various database types.
•	We created our environment file and ensured each of us had a unique signing_key.
•	Developed the PoolOwner and SignIn models.
•	Successfully launched Docker containers, images, and volumes, confirming localhost:8000 is operational.
May 29, 2024
•	Restarted the project from scratch.
May 30, 2024
•	Completed authentication for sign in, sign up, authentication, and sign out functionalities.
•	Reviewed React hooks.
•	Created our first pool. Mark and I coded the model, routes, and migrations.
May 31, 2024
•	Enhanced the days_available functionality within our database.
•	Completed FastAPI functionalities for creating and deleting pools.
•	Consulted with Kyle on training for G-Force endurance and practiced breathing exercises.
•	Set up pgAdmin.
June 3, 2024
•	Implemented the update pool router, model, and queries.
•	Refined our database to handle joining tables.
•	Worked on DBfiddle mappings.
•	Developed our database diagram.
June 4, 2024
•	Finished CRUD operations for pools, specifically fetching all pools.
•	Created the Amenities tables, models, and routers, and tested them on FastAPI.
•	Downloaded all lectures and uploaded them to Dropbox.
June 5, 2024
•	Reviewed Big-O notation and algorithms, including bubble sort.
•	Completed the amenities ID association with pool creation for owners.
•	Finalized the get reservation router and model, ensuring FastAPI functionality.
June 6, 2024
•	Completed the amenities code, incorporating proper logic for ID tables.
•	Finished the post request for reservations, with remaining CRUD operations to be finalized.
June 7, 2024
•	Attended my brother's wedding.
June 10, 2024
•	Reviewed unit testing concepts.
•	Created the test_reservations_get_all unit test and assisted group members with theirs.
June 11, 2024
•	Initiated coding for Redux endpoints, removed front-end authentication, and began implementing the new auth system.
June 12, 2024
•	Merged Redux endpoints into the main branch.
•	Added a backend feature to retrieve reservations by pool ID.
•	Began working on the navigation bar and setting up components for endpoint testing.
•	Explored and installed Tailwind CSS.
•	Met with my career counselor.
June 13, 2024
•	Pair programmed with James to complete the Pool Profile page, including front-end delete functionality.
•	Completed 80% of the navigation bar.
•	Finalized SignIn, SignUp, and CreatePool forms.
•	Installed necessary Tailwind components and updated Excalidraw.
June 17, 2024
•	Updated the authenticated user code to display only the pools created by the user on the My Profile page, including new query, router, and JSX slice additions.
•	Took a practice assessment.
•	Fixed button redirects.
•	Completed pool details feature, enabling image click redirection on the My Profile page.
June 18, 2024
•	Finalized 90% of our CSS styling.
•	Completed two merge requests and significantly updated the App.css file.
•	Engaged in pair and mob coding sessions.
June 19, 2024
•	Troubleshot and fixed sign up and sign in page discrepancies.
•	Completed the carousel feature.
June 20, 2024
•	Focused on error catching and handling.
•	Finalized most of the CSS styling.
•	Initiated deployment tasks.
June 21, 2024
•	Resolved duplicate reservations issue.
•	Conducted stress tests.
•	Reviewed MVP readiness for submission.
•	Organized stretch goal objectives.
June 24, 2024
•	Finishing the Calander branch and merging it with main
•	We are all updating journals, reviewing the readMe and submitting out project
•	I am doing the initialization

