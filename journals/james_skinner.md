## Journals

# Journal

## 05/28

**What did we work on the day before?**
- Git

**What do we want to get done today?**
- Adding user functions, more than just the basics.
- See what everyone has to say.

**What are our blockers?**
- Time

**Discussion Points:**
- Which database do we want to use? PostgreSQL
- Go over what we are doing today:
  - Nelly for the Git tutorial and practice
  - Have everyone go through the journal aspect
  - Start coding?
  - Where do we start? Pool_owner

**Order of Leading and Coding:**
- Hasan / Cosimo
- Mark / James
- James / Mark
- Cosimo / Hasan

**Things we did:**
- Ran through Git flow and practiced to better understand the flow of requests and teamwork in GitLab.
- Created an .env file to connect to localhost 8000 and see our Fast API functionality.

## 05/29

**What did we work on the day before?**
- Worked on Git and Git flow, started the user pathway.

**What do we want to get done today?**
- Figure out the remaining paths for our user and flush out our database.

**What are our blockers?**
- Time and understanding what we are doing moving forward.

**Additional Notes:**
- We couldn't add a user pathway due to deep code issues, regressed to an earlier version of our project.
- Used the day's lecture to get the sign-up to work and saved our progress.
- Learned a lot about mob coding and handling this type of coding.

## 05/30

**What did we work on the day before?**
- Got our signup to function in FastAPI.

**What do we want to get done today?**
- Finish our functions for FastAPI, build both pool and user models.

**What are our blockers?**
- Talking over each other and overwhelming the coder.

**Additional Notes:**
- Managed to get our signup function to work, then built our sign-in earlier in the day.
- Focused on other parts of the application, including adding pools, but couldn't see the list of pools.
- Worked until 7:30 PM.

## 05/31

**What did we work on the day before?**
- Worked on fleshing out the pools model and added a pool.

**What do we want to get done today?**
- Finalize the delete function on the pool.

**What are our blockers?**
- Time and diverging from the agreed-upon path.

**Additional Notes:**
- Came together well on the project and worked well with each other.
- Flushed out the delete function for the pool and updated everyone on the database sign-in.

## 06/03

**Discussion Points:**
- Do we want to use Redux?
- Complete the update function for pools.
- Look over and finalize the database and DB Fiddle.
- Set up the database.

**What did we work on the day before?**
- Finished the delete function for the pool and worked on the database.

**What do we want to get done today?**
- Finish both the database setup and the update for pools.

**What are our blockers?**
- Time and staying on track.

**Additional Notes:**
- Started off rough as we struggled to add the update button.
- With Kyle's help, we got the update to work.
- Ended the day by writing out the amenities for the database.

**Weekly Goals:**
- **Monday:** Finalize the database.
- **Tuesday:** Reservations/Amenities/API.
- **Wednesday:** Finish up back-end.
- **Thursday:** Intro to Redux, intro to front-end.
- **Friday:** Work further on front-end, complete CRUD functions for reservations.

## 06/04

**What did we work on the day before?**
- Mapped out CRUD functions for pools.

**What do we want to get done today?**
- Finish CRUD for pools (get all), figure out res/amen (CRUD functions), do table for migrations for reservations.

**What are our blockers?**
- Time and not staying on topic.

**Additional Notes:**
- Created a get-all CRUD function for pools.
- Started building migrations for amenities and reservations.
- Implemented the GET ALL and ID CRUD for amenities and connected it to pools.

## 06/05

**What did we work on the day before?**
- Flushed out pool functions and got GET ID for amenities CRUD function.

**What do we want to get done today?**
- Get help from Riley to add amenities to pools. Decide if we are going to build out reservations.

**What are our blockers?**
- No blockers.

**Additional Notes:**
- Got pseudo code from Riley to help add amenities to pools.
- Started working on reservations and completed the POST CRUD function.

## 06/06

**Discussion Points:**
- Do we want to mob code for testing? Yes, and we'll do it together on Monday.

**What did we work on the day before?**
- Worked on part of mixing amenities with pools but didn't flush it out. Started on reservations.

**What do we want to get done today?**
- Fix the create function for reservations, flesh out amenities showing up under pools, do as many CRUD functions on reservations as possible.

**What are our blockers?**
- Cosimo leaving at 5 PM.

**Additional Notes:**
- Flushed out amenities and now they populate under pools.
- Made updates to amenities reflecting on pools for delete and update.
- Completed the POST CRUD function for reservations.

## 06/07

**What did we work on the day before?**
- Flushed out amenities and populated them under pools. Made updates to amenities reflecting on pools.

**What do we want to get done today?**
- DELETE a pool with amenities. CRUD functions for reservations (GET ALL, DELETE, GET ID).

**What are our blockers?**
- No blockers.

**Additional Notes:**
- Got the amenities delete function to work, deleting both the amenities and the pool.
- Completed all CRUD functions for reservations, effectively finishing our back-end.

## 06/10

**What did we work on the day before?**
- Finished all back-end work, including amenities and reservations.

**What do we want to get done today?**
- Finish the testing requirements for the group. Start front-end (Tailwind CSS, Redux).

**What are our blockers?**
- No blockers.

**Additional Notes:**
- Got everyone to perform a test and wrapped up testing.
- Started Redux setup and made significant progress but still need to finish the install.

**Weekly Goals:**
- **Monday:** Testing completed.
- **Tuesday:** Work on front-end (Tailwind, Redux).
- **Wednesday:** Continue with front-end, finish Redux and Tailwind.
- **Thursday:** Start pair programming.
- **Friday:** Off.

## 06/11

**What did we work on the day before?**
- Finished back-end, completed testing, and started Redux setup.

**What do we want to get done today?**
- Finish Redux setup and get authentication for the front-end set up.

**What are our blockers?**
- No blockers.

**Additional Notes:**
- Set up most of Redux.
- Had some issues with direction and didn't fully flesh out endpoints.

## 06/12

**(Cosimo - Leading) (Mark - Coding)**

**What did we work on the day before?**
- Set up Redux but didn't fully flesh out endpoints.

**What do we want to get done today?**
- Flesh out endpoints, start Tailwind, and start components (nav bar).

**What are our blockers?**
- No blockers.

**Additional Notes:**
- Got all endpoints for Redux but need to complete component functionality.
- Added additional features for retrieving reservations in pools.
- Installed Tailwind and began CSS setup.

## 06/13

**(Hasan - Leading) (James - Coding) (Cosimo - Timer/Keeping Pace)**

**What did we work on the day before?**
- Built endpoints for Redux and partially touched components like sign-up but didn't fully flesh them out.
- Mapped out back-to-front functionality for retrieving reservations under pools.
- Installed Tailwind but didn't complete the setup.

**What do we want to get done today?**
- Pair program component functionality: James and Cosimo, Hasan and Mark.

**What are our blockers?**
- No blockers.

**Additional Notes:**
- Added more CSS from Tailwind to the nav bar and set up theme colors for the entire website.

## 06/17

**What did we work on the day before?**
- Built out all functions but not all component functionality.

**What do we want to get done today?**
- Get a significant amount of components done before Tuesday to reassess the week.
- Plan the week based on the amount of work needed.
- Get functionality for all components.
- Build out CSS.
- Work through and generate new issues.

**What are our blockers?**
- New to pair programming.

**Order of Leading and Coding:**
- James - Leading

**Weekly Goals:**
- **Monday:** Work on components.
- **Tuesday:** Work on components and reassess for the week.
- **Wednesday:** Off.
- **Thursday:** Stress test, finish CSS, deployment.
- **Friday:** Stretch goals.

**Additional Notes:**
- Worked on authentication, sign-up, and sign-out redirects.
- Pool detail functionality, reservations table, and updated CSS.
- Picture links to pool detail page.
- Authentication shows only the user's pools.
- Significant CSS progress through pair programming.

## 06/18

**What did we work on the day before?**
- Got authentication to show only the user's pools.
- Redirected sign-out and sign-up pages.
- Completed most functionality.

**What do we want to get done today?**
- Continue working on functionality and issues.
- Start the framework for CSS.

**What are our blockers?**
- No blockers.

**Order of Leading and Coding:**
- Mark - Leading

**Additional Notes:**
- Most functionality is complete with minor details needing attention.
- Most CSS is done across the website, needs duplication for unfinished components.

## 06/20

**What did we work on the day before?**
- Fixed the refresh issue.
- Created an error box for the same username.
- Looked into deployment.
- Fixed phone number display.
- Fixed the carousel.
- Troubleshot Docker deletion issues.

**What do we want to get done today?**
- Fix duplicate reservations issue.
- Stress test.
- Review MVP and prepare for submission.
- Set and organize stretch goals.

**What are our blockers?**
- Cosimo leaving early.

**Additional Notes:**
- Encountered bugs and issues, hindering progress.
- Spent time ensuring everyone's code worked, resolved major issues but at the cost of expected progress.

## 06/21

**(Hasan Leading)**

**What did we work on the day before?**
- Fixed the refresh issue.
- Created an error box for the same username.
- Reviewed steps for deployment.
- Fixed phone number issue.
- Fixed carousel.
- Troubleshot Docker issues.

**What do we want to get done today?**
- Fix duplicate reservations issue.
- Stress test.
- Review MVP for submission.
- Remove dead code or comments.
- Finish ReadMe.
- Set and start on stretch goals.

**What are our blockers?**
- Cosimo leaving early.

**Additional Notes:**
- Aiming to finish by 5-6 PM.
- Reviewed MVP to ensure all points are covered.
- Planned to get feedback from Delonte after the stress test.
- Set and started on stretch goals.

---

**06/22**

**(Hasan Leading)**

**What did we work on the day before?**
- Fixed the refresh issue.
- Added an error box for duplicate usernames.
- Reviewed steps for deployment.
- Fixed phone number issues.
- Fixed the carousel.
- Troubleshot Docker deletion issues.

**What do we want to get done today?**
- Fix duplicate reservations issue.
- Stress test.
- Review MVP for submission.
- Organize stretch goals.
- Finalize the ReadMe.

**What are our blockers?**
- Cosimo leaving early.

**Additional Notes:**
- Aiming to finish by 5-6 PM.
- Reviewed MVP to ensure all points are covered.
- Planned to get feedback from Delonte after the stress test.
- Set and started on stretch goals.

---

**06/24**

**(James Leading)**

**Project submission day**
- Button up project
- Submit project
