## Journals

Please keep all of your individual journals in this directory.

Each team member is required to keep a development journal, which should be a single markdown file with an entry for each day the project was worked on.

Recommended naming convention is `first_last.md`.

05/24/24

- We got our repository today and we started to get our intro to how to set up the website. We covered several topics such as how we will have share information.

05/28/24

- We decided to use postgreSQL as our database. We also practiced git flow, including issues to help stay organized.

05/29/24

- what did you work on the day before
	- we worked on git and git flow and started the user pathway
- what do we want to get done today?
	- figure out the remaining paths for our user and flushing out our database.
- what are out blockers?
	- blockers are time and understanding of what we are doing moving forward.

- We were not able to add a user pathway as we got so deep into the code issues that we ended up regressing to an earlier version of our project. We then were able to use our lecture from the day to then get sign up to work and got to a good place where we then saved for the day. We learned a lot about mob coding today and how to better handle this type of coding.


05/30/24

- what did you work on the day before?
	- we got our sign up to function in fastAPI
-  what do we want to get done today?
	- finish out our functions for fastAPI, build both pool and user models
- what are out blockers?
	- talking over each other and overwhelming the person that is coding.

- We were able to get our sign up function to work and then build out our sign in earlier in the day. We used the extra time to focus on other parts of the application including adding pools but were only able to make the pools but not see the list of pools.

06/03/24

- Complete the update function for pools.
- Look over and finalize the database and db fiddle.
- Set up the database

-what did you work on the day before?
	-  we finished the delete function for pool and worked on database
-what do we want to get done today?
	- finish out both dabs and update for pools
-what are out blockers?
	-  time and staying on track

We started off a little rough as we weren’t able to come together on how to move forward with adding the update button. After help from Kyle we were able to finally get update to work. We ended the day with writing out the amenities for the database.

Weekly Planned Progress

Monday - Finalize data base
Tuesday - Reservations/Amenities/API
Wednesday - Finish up back end
Thursday - Intro to Redux, intro to front end
Friday - Work further on Front End

06/04

-what did you work on the day before?
	-  All crud functions for reservations
	-  Pool owner needs to be able to see what reservations have for their pool
	-  Two end points for reservations including the id to get the pool
	-  Amenities and days of the week
-what do we want to get done today?
	- finish crud for pools (get all), figure out reservations and amentities (crud functions),
	-  Do table for migrations for reservations
-what are out blockers?
	-  Time and not staying on topic

We started by creating a get all crud function for pools, this has effectively designed all of the crud functions for pool. We then started building out the migrations for amenities and reservations. We ended the day with implementing the GET ALL and ID crud for amenities and then connecting it to pools.

06/05

-what did you work on the day before?
	-  yesterday we flushed out the pool functions  in it’s entirety and then we were able to get the GET ID for amenities crud function
-what do we want to get done today?
	- get help from Riley to add amenities to pools. Decide if we are going to build out reservations
-what are out blockers?
	-   no blockers

We were able to get some pseudo code from Riley to help us add amenities to pools but did not fully flush out the functions to see amenities under pools yet. Since we did not have Riley at the end of the day, we then started working on reservations and were able to get the crud function POST completed.

06/06

Do we want to mob code for the testing?
Yes and we will do it on Monday together.


-what did you work on the day before?
	-  worked on part of mixing amenities with pools but did not flush it out. We started on reservations.
-what do we want to get done today?
	- fix create function for reservations
	- flush out amenities showing up under pools.
	- do as many crud functions on reservations if we have time
-what are out blockers?
	-  Cosimo leaving at 5pm

We were able to flush out amenities and now can get them to populate under pools. We also are able to make updates to amenities and have it reflect on pools pertaining to delete and update. We were also able to get the post crud function reservations. Function finished on reservations.

06/07

-what did you work on the day before?
- flushed out amenities and populate under pools,
- updates to amenities and have it reflect on pools pertaining to update
-what do we want to get done today?
- DELETE a pool with amenities
- Crud functions for reservations. (GET ALL, DELETE, GET by ID)
-what are out blockers?
- No blockers

We were able to get the amenities delete function to work so that it will delete the amenities as well as the pool. We also started doing the crud functions for reservations and were able to get all crud functions finished for reservations. This effectively finishes our back end.

06/10

-what did you work on the day before?
- Effectively got all the back end done. This means that we flushed out both amenities and reservations.
-what do we want to get done today?
- Finish the testing requirements for the entire group
- Start front end (tailwind css, redux)
-what are out blockers?
- No blockers

We were able to get everyone to perform a test and able to wrap that portion up. We then started redux set up and were able to get through about 80% done but still need to flush out the install.

Monday - Testing completed
Tuesday - Work on front end (tailwind, redux)
Wednesday - Get started with front end, continue to finish redux and tailwind
Thursday - Start pair programming
Friday - Off

06/11

-what did you work on the day before?
- We were able to finish up the back end, get everyone to do a test. We also started our redux but have not finished set up yet.
-what do we want to get done today?
- Finish up redux, and get auth for front end set up
-what are out blockers?
- No blockers

We were able to set up most of redux today. We had a little bit of issues on the direction of where we wanted to go and did not fully flush out the end points.

Leading		Coding
Hasan		Cosimo
Mark		James
James		Mark
Cosimo		Hasan

06/12

(Cosimo - leading) (Mark - coding)

-what did you work on the day before?
- Set up redux but were not able to fully flush out end points.
-what do we want to get done today?
- We want to flush out endpoints, start tailwind and start components (nav bar)
-what are out blockers?
- No blockers

We were able to get all our end points for redux, but do not have all the components fully flushed out with functionality. We were able to add additional features from the back that brings us reservations in pools. Finally we installed tailwind and looked at what starting up that css looks like for our app.

(Hasan - leading) (James - coding) (Cosimo -Timer/keeping pace)

06/13

-what did you work on the day before?
- We built end points for redux. partially touched Components such as sign up but do not have them fully flushed out. We mapped out from back to front functionality for retrieving reservations under pool. Installed tailwind but did not flush it out.
-what do we want to get done today?
- Pair program the functionality of the components. James and Cosimo/ Hasan and Mark.
-what are out blockers?

-We added more css from tailwind to the nav bar, and set up theme colors to reference for the entire website.

06/17

James - leading

-what did you work on the day before?
- We were able to build out all functions but not functionality of all components
-what do we want to get done today?
- Get big amount of components done before Tuesday to reassess for the remainder of the week. Plan the remainder of the week as pertaining to amount of work we need to get done.
- Get functionality of all components
- Build out CSS
- Work through issues generated, generate new issues.
-what are our blockers?
- New to pair programming.

- If trying to authenticate, it will show you nothing.
- Helped get sign up, sign out to redirect to appropriate pages
- Pool detail is done with all functionality
    - Reservations table
    - Updated CSS
    - Picture will take you to detail page of pool
- Authentication for sign in to only populate pools of person logged in

We were able to get a significant amount of CSS done. We utilized pair programming and were able to use issues to then knock out the project piece by piece while simultaneously creating new issues we run into them. We also were able to get all rerouting done for all pages.


Moved to pair programming

Monday	- Work on components
Tuesday	- Work on components and reassess if what we need to do for the week.
Wednesday - Off
Thursday - stress test/finish out CSS - deployment
Friday	- stretch goals

06/18

Mark - leading

-what did you work on the day before?
- Got authenticate to allow you to see the only your pools
- Redirected the sign out and sign up redirect web pages
- Most functionality is finished
-what do we want to get done today?
- Keeping pushing on functionality and working issues
- Started on framework for css
-what are our blockers?
- No blockers

- Functionality is fully complete with minor detail still needing touches (issues)
- Most CSS across the website is finished, just need to duplicate to other component that are not finished.

06/20

Cosimo - leading

-what did you work on the day before?
- People came in on
-what do we want to get done today?
- Fix refresh issue
- Stress test
- Have error catching box
- Work on deployment
- Fix phone number display
- Pop up for already existing user
-what are our blockers?
- Cosimo leaving at 230

Things to do for deployment
- Run eslint for jsx
- Reservations can’t overlap

Today was a tough day all around. While we continued to knock out issues and work on anything that would pop up during that process, we also ran into bugs that would either break the code or create issues for one but not all of us. We spent a lot of the day trying to get each other’s code to work. We managed to get everyone to a good place by the end of the day and developed solutions for these issues that would most likely have come up later, but it was at a cost of not getting the amount of work done that we are accustomed to accomplishing.

06/21

Hasan - Leading

-what did you work on the day before?
- Fixed refresh issue
- Error box for same user name
- Looked at what it takes to deploy
- Fix phone number issue
- Fixed carousel
- Trouble shoot issues with deleting docker
-what do we want to get done today?
- Fix duplicate reservations issue
- Stress test
- Look over MVP and see if we can submit
- Organize on what we want to accomplish for stretch goals
-what are our blockers?
- Cosimo leaving at will be gone a couple hours.
- Reassess but would like to be done by 5-6 pm

06/24

James - Leading

-what did you work on the day before?
- Fixed refresh issue
- Error box for same user name
- Looked at what it takes to deploy
- Fix phone number issue
- Fixed carousel
- Trouble shoot issues with deleting docker
-what do we want to get done today?
- Fix duplicate reservations issue
- Stress test
- Look over MVP and see if we can submit
- Organize on what we want to accomplish for stretch goals
-what are our blockers?
- Cosimo leaving at will be gone a couple hours.
- Reassess but would like to be done by 5-6 pm

We are mostly done with the app. We need to put final touches such as journaling and being able to start the app from the ReadMe instructions. We wil hand in the app by the end of the day today. Bittersweet but it was a columniation of a lot of work between the four of us and I am proud.
